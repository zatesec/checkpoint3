# nginx state for serving content
FROM nginx:alpine
# Set working directory to nginx asset directory
#WORKDIR /usr/share/nginx/html
# Remove default nginx static assets
RUN rm -rf /usr/share/nginx/html/*
# Copy static assets over
COPY ./ /usr/share/nginx/html
COPY ./default.conf.template /etc/nginx/templates/
ENV NGINX_PORT=8080
RUN rm -rf /usr/share/nginx/html/.gitlab-ci.yaml /usr/share/nginx/html/Dockerfile
RUN mkdir -p /usr/share/nginx/html/_ah && \
    echo "healthy" > /usr/share/nginx/html/_ah/health
